import { StackNavigator } from 'react-navigation'
import Newscreen1 from '../Containers/Newscreen1'
import LaunchScreen from '../Containers/LauchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  Newscreen1: { 
    screen: Newscreen1 
  },
  LaunchScreen: { 
    screen: LaunchScreen }
})

export default PrimaryNav
