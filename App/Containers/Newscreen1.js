import React, { Component } from 'react'
import { Text, TextInput, KeyboardAvoidingView, Autocomplete, Image, View, Button } from 'react-native'
import { connect } from 'react-redux'
import { Images } from '../Themes'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'


// Styles
import styles from '../Containers/Styles/Newscreen1Style'
export default class NewScreen extends Component {
  state = {
    behavior: 'padding',
    };

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        

      <View style={styles.container}>
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
      <TextInput
         underlineColorAndroid={'#ffffff'}
         placeholder="Search your location!"
         style={styles.sectionText} />
         </KeyboardAvoidingView>
      </View> 

      
     <View style={styles.buttonContainer} >
    
          <Button  
              onPress={() => this.props.navigation.navigate('  MyFavorite  ')}
              title="Favorite Parking"
                />
        
          <Button 
              onPress={() => this.props.navigation.navigate('  Dashboard  ')}
              title="My Dashboard"
                />
      
      </View>
    </View>
      
    )
  }
}


const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

//export default connect(mapStateToProps, mapDispatchToProps)(NewScreen)
