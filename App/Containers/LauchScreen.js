import React, { Component } from 'react'
import { ScrollView, Text, Image, View, Button, ImageButton, TouchableOpacity } from 'react-native'
import { Images } from '../Themes'


// Styles
import styles from '../Containers/Styles/LanchScreenStyles'

export default class LaunchScreen extends Component {
  
  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <View style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch1} style={styles.logo} />
            <Image source={Images.ready} />
          </View>

          <View style={styles.section} >
            <Text style={styles.sectionText}>
              Welcome!{this.props.name=' Franky Ho'}
            </Text>
          </View>
          </View>
          
          <View style={{flexDirection: 'row', justifyContent: 'space-around', height:400}} >
          
          <View style={styles.container}>
            <Image source={Images.license} />
            <Text style={styles.buttonText}>
              License Spaces
            </Text>
          </View>

            <Image source={Images.line} />
        
          <View style={styles.container}>
           
            <TouchableOpacity
                    /*onPress={() => this.props.navigator.push(route())}*/
                onPress={() => this.props.navigaton.navigate('Newscreen1')}
                activeOpacity={0.6}
            >
            <Image source={Images.parking} /> 
        
          </TouchableOpacity>
          </View>
            

      
        </View>
      </View>



      
    )
  }
}
