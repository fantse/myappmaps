/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from '../../../../../Users/RN/AppData/Local/Microsoft/TypeScript/2.9/node_modules/@types/react';
import {StyleSheet, Text, View} from 'react-native';
import MapView from 'react-native-maps';




export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>

        <MapView style={styles.map}
          region={{
            latitude: 22.3362513,
            longitude:  114.1973951,
            latitudeDelta: 0.1, 
            longitudeDelta: 0.1

          }}>
        </MapView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right:  0,
      justifyContent: 'flex-end',
      alignItems: 'center'
  },
  map:  {
      position: 'absolute',
      top:  0,
      left: 0,
      bottom: 0,
      right:  0,

  }
  
});
